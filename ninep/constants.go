package ninep

const (
	NoTag = 0xffff
	NoFid = 0xffffffff

	MaxWElem = 16
)

// Mode bits
const (
	OEXCL = 0x00001000
	DMDIR = 0x80000000
)
