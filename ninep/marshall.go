package ninep

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"reflect"
)

type Tag uint16
type Fid uint32

type Qid struct {
	Typ     byte
	Version uint32
	Path    uint64
}

type Stat struct {
	Size uint16

	// The plan 9 kernel uses the same struct for both the 9p wire
	// protocol and it's own internal representation. These two fields
	// are meaningful only to the latter, but space is reserved for them
	// in the message.
	dev uint16
	typ uint32

	Qid    Qid
	Mode   uint32
	Atime  uint32
	MTime  uint32
	Length uint64
	Name   string
	Uid    string
	Gid    string
	MUid   string
}

// Message types
const (
	Tversion = 100
	Rversion = iota
	Tauth
	Rauth
	Rerror = 105 // Terror is invalid, but there's a code reserved for it
	Tflush = iota
	Rflush
	Tattach
	Rattach
	Twalk
	Rwalk
	Topen
	Ropen
	Tcreate
	Rcreate
	Tread
	Rread
	Twrite
	Rwrite
	Tclunk
	Rclunk
	Tremove
	Rremove
	Tstat
	Rstat
	Twstat
	Rwstat
)

var ShortReadErr = fmt.Errorf("Short Read")

func BadOpErr(opcode uint8) error {
	return fmt.Errorf("Invalid message Opcode: %d", opcode)
}

// I'm still really unsatisified with the way this and the opcode function
// below it are expressed. Keep thinking about how this is should be done.
var (
	op2Type = map[uint8]reflect.Type {
		Tversion : reflect.TypeOf(TVersion{}),
		Rversion : reflect.TypeOf(RVersion{}),
		Tauth    : reflect.TypeOf(TAuth{}),
		Rauth    : reflect.TypeOf(RAuth{}),
		Rerror   : reflect.TypeOf(RError{}),
		Tflush   : reflect.TypeOf(TFlush{}),
		Rflush   : reflect.TypeOf(RFlush{}),
		Tattach  : reflect.TypeOf(TAttach{}),
		Rattach  : reflect.TypeOf(RAttach{}),
		Twalk    : reflect.TypeOf(TWalk{}),
		Rwalk    : reflect.TypeOf(RWalk{}),
		Topen    : reflect.TypeOf(TOpen{}),
		Ropen    : reflect.TypeOf(ROpen{}),
		Tcreate  : reflect.TypeOf(TCreate{}),
		Rcreate  : reflect.TypeOf(RCreate{}),
		Tread    : reflect.TypeOf(TRead{}),
		Rread    : reflect.TypeOf(RRead{}),
		Twrite   : reflect.TypeOf(TWrite{}),
		Rwrite   : reflect.TypeOf(RWrite{}),
		Tclunk   : reflect.TypeOf(TClunk{}),
		Rclunk   : reflect.TypeOf(RClunk{}),
		Tremove  : reflect.TypeOf(TRemove{}),
		Rremove  : reflect.TypeOf(RRemove{}),
		Tstat    : reflect.TypeOf(TStat{}),
		Rstat    : reflect.TypeOf(RStat{}),
		Twstat   : reflect.TypeOf(TWStat{}),
		Rwstat   : reflect.TypeOf(RWStat{}),
	}
)

func opCode(msg interface{}) uint8 {
	switch msg.(type) {
	case TVersion:
		return Tversion
	case RVersion:
		return Rversion
	case TAuth:
		return Tauth
	case RAuth:
		return Rauth
	case RError:
		return Rerror // Terror is invalid, but there's a code reserved for it
	case TFlush:
		return Tflush
	case RFlush:
		return Rflush
	case TAttach:
		return Tattach
	case RAttach:
		return Rattach
	case TWalk:
		return Twalk
	case RWalk:
		return Rwalk
	case TOpen:
		return Topen
	case ROpen:
		return Ropen
	case TCreate:
		return Tcreate
	case RCreate:
		return Rcreate
	case TRead:
		return Tread
	case RRead:
		return Rread
	case TWrite:
		return Twrite
	case RWrite:
		return Rwrite
	case TClunk:
		return Tclunk
	case RClunk:
		return Rclunk
	case TRemove:
		return Tremove
	case RRemove:
		return Rremove
	case TStat:
		return Tstat
	case RStat:
		return Rstat
	case TWStat:
		return Twstat
	case RWStat:
		return Rwstat
	}
	panic("Invalid message") // XXX: TODO: handle this better
}

const (
	sizeTagSize = 4 // number of bytes used for the size of the entire
					// message, within the message itself
	opCodeSize = 1  // number of bytes used for the 
)

func WriteMsg(w io.Writer, msg interface{}) {
	var buf bytes.Buffer
	code := opCode(msg)
	binary.Write(&buf, binary.LittleEndian, msg)
	binary.Write(w, binary.LittleEndian, uint32(sizeTagSize + opCodeSize + buf.Len()))
	binary.Write(w, binary.LittleEndian, code)
	buf.WriteTo(w)
}

func ReadMsg(r io.Reader) (interface{}, error) {
	var size uint32
	err := binary.Read(r, binary.LittleEndian, &size)

	buf := make([]byte, size-sizeTagSize)	// XXX: BLOCKER: We need to check that
										    // size is not too big before allocating
									        // memory. A mallicious user could exploit
									        // this to ask us to allocate way more
											// memory than we can really afford.
	n, err := r.Read(buf)

	if err != nil {
		return nil, err
	} else if n < len(buf) {
		return nil, ShortReadErr // XXX: It's not really okay to assume
								 // that a short read means the rest of
								 // the data isn't coming. This should
								 // be handled in a better way.
	}

	typ := op2Type[buf[0]]
	if typ == nil {
		return nil, BadOpErr(buf[0])
	}

	msg := reflect.New(typ).Interface()
	binary.Read(bytes.NewBuffer(buf[1:]), binary.LittleEndian, msg)
	return msg, nil
}

type TVersion struct {
	Tag     Tag
	Msize   uint32
	Version string
}

type RVersion struct {
	Tag     Tag
	Msize   uint32
	Version string
}

type TAuth struct {
	Tag   Tag
	Afid  Fid
	Uname string
	Aname string
}

type RAuth struct {
	Tag  Tag
	Aqid Qid
}

type RError struct {
	Tag   Tag
	Ename string
}

type TFlush struct {
	Tag    Tag
	OldTag Tag
}

type RFlush struct {
	Tag Tag
}

type TAttach struct {
	Tag   Tag
	Fid   Fid
	Afid  Fid
	Uname string
	Aname string
}

type RAttach struct {
	Tag Tag
	Qid Qid
}

type TWalk struct {
	Tag    Tag
	Fid    Fid
	Newfid Fid
	nwname uint16
	Wname  []string
}

type RWalk struct {
	Tag   Tag
	nwqid uint16
	Wqid  Qid
}

type TOpen struct {
	Tag  Tag
	Fid  Fid
	Mode byte
}

type ROpen struct {
	Tag    Tag
	Qid    Qid
	IOUnit uint32
}

type TCreate struct {
	Tag  Tag
	Fid  Fid
	Name string
	Perm uint32
	Mode uint8
}

type RCreate struct {
	Tag    Tag
	Qid    Qid
	IOUnit uint32
}

type TRead struct {
	Tag    Tag
	Fid    Fid
	Offset uint64
	Count  uint32
}

type RRead struct {
	Tag   Tag
	count uint32
	Data  []byte
}

type TWrite struct {
	Tag    Tag
	Offset uint64
	count  uint32
	Data   []byte
}

type RWrite struct {
	Tag   Tag
	Count uint32
}

type TClunk struct {
	Tag Tag
	Fid Fid
}

type RClunk struct {
	Tag Tag
}

type TRemove struct {
	Tag Tag
	Fid Fid
}

type RRemove struct {
	Tag Tag
}

type TStat struct {
	Tag Tag
	Fid Fid
}

type RStat struct {
	Tag  Tag
	Stat Stat
}

type TWStat struct {
	Tag  Tag
	Fid  Fid
	Stat Stat
}

type RWStat struct {
	Tag Tag
}
