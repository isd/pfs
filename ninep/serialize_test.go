package ninep

import "testing"

type NullW struct{}

func (nw NullW) Write(p []byte) (n int, err error) {
	return len(p), nil
}

func TestWrite(t *testing.T) {
	msg := TVersion{Tag: 1, Msize: 2 << 16, Version: "9P2000"}
	WriteMsg(NullW{}, msg)
}
