#!/bin/sh
#
# Pre-commit hook
#
# The gofmt tool is great, but I'd rather not have to run it manually every
# commit. Instead, let's just run it on the whole source tree before each
# commit.
find * -name '*.go' | tee /dev/stderr | xargs gofmt -w
